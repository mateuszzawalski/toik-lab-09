package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.services.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    private final MovieService movieService;

    public MovieApiController() {
        movieService = new MovieService();
    }

    @GetMapping("/api/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", movieService.getMovies());
        return ResponseEntity.ok().body(movieService.getMovies());    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PutMapping("/api/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable Integer id, @RequestBody CreateMovieDto createMovieDto) {

        boolean isEdited = movieService.editMovie(id, createMovieDto);

        if(!isEdited){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();

    }

    @PostMapping("/api/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- id: {}", createMovieDto.getMovieId());
        LOG.info("--- title: {}", createMovieDto.getTitle());

        boolean isAdded = movieService.addMovie(createMovieDto);

        if(!isAdded){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.created(new URI("/movies/" + createMovieDto.getMovieId())).build();
    }

    @DeleteMapping("/api/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable Integer id){

        boolean isDeleted = movieService.deleteMovie(id);

        if(!isDeleted){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}

package com.demo.springboot.services;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;

public interface MovieServiceInterface {
    MovieListDto getMovies();
    boolean addMovie(CreateMovieDto movie);
    boolean editMovie(Integer id, CreateMovieDto movie);
    boolean deleteMovie(Integer id);
}

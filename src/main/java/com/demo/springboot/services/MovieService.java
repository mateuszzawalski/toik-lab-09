package com.demo.springboot.services;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;

import java.util.*;

public class MovieService implements MovieServiceInterface{

    private final List<MovieDto> movies;
    private int currentId;

    public MovieService() {
        movies = new ArrayList<>();
        currentId = 0;
    }


    @Override
    public MovieListDto getMovies() {

        Comparator<MovieDto> compareById = Comparator.comparing(MovieDto::getMovieId);

        movies.sort(compareById.reversed());

        return new MovieListDto(movies);
    }

    @Override
    public boolean addMovie(CreateMovieDto movie) {

        if(!movie.getTitle().isEmpty() && movie.getYear() != null && !movie.getImage().isEmpty()){
            currentId++;

            movie.setMovieId(currentId);

            movies.add(new MovieDto(movie.getMovieId(), movie.getTitle(), movie.getYear(), movie.getImage()));


            return true;
        }else{
            return false;
        }

    }

    @Override
    public boolean editMovie(Integer id, CreateMovieDto movie) {

        Optional<Integer> index = findIndex(id);

        if(index.isPresent()){

            if(movie.getTitle() != null && !movie.getTitle().isEmpty()){
                movies.get(index.get()).setTitle(movie.getTitle());
            }
            if(movie.getImage() != null && !movie.getImage().isEmpty()) {
                movies.get(index.get()).setImage(movie.getImage());
            }
            if(movie.getYear() != null) {
                movies.get(index.get()).setYear(movie.getYear());
            }
        }else{
            return false;
        }

        return true;

    }

    @Override
    public boolean deleteMovie(Integer id) {

        Optional<Integer> index = findIndex(id);

        if(index.isPresent()){

            movies.remove(index.get().intValue());

        }else{
            return false;
        }

        return true;


    }

    private Optional<Integer> findIndex(Integer id){
        int i = 0;
        for( MovieDto m : movies){
            if(m.getMovieId().equals(id)){
                return Optional.of(i);
            }else{
                i++;
            }
        }
        return Optional.empty();
    }
}
